import { MessageInput } from '@/Components/Common/MessageInput';
import { PasswordField } from '@/Components/Common/PasswordField';
import { SearchBar } from '@/Components/Common/SearchBar';
import { TextField } from '@/Components/Common/TextField';
import { GrayColors, screenWidth } from '@/Utils';
import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';

interface Params {
  username: string;
  password: string;
  message: string;
  search: string;
}

export const InputGroup = () => {
  const [params, setParams] = useState<Params>({
    username: 'quannh1',
    password: 'password',
    message: 'good morning~~~',
    search: '회사명은 무엇',
  });
  const handleChangeText = (keyName: string, value: string) => {
    setParams({
      ...params,
      [keyName]: value,
    });
  };

  return (
    <View style={styles.container}>
      <View style={styles.inputItem}>
        <TextField
          keyName="username"
          placeholder="회사 이메일을 입력해주세요"
          value={params.username}
          onTextChange={handleChangeText}
          editable={true}
        />
      </View>

      <View style={styles.inputItem}>
        <PasswordField
          keyName="password"
          placeholder="회사 이메일을 입력해주세요"
          value={params.password}
          onTextChange={handleChangeText}
          editable={true}
        />
      </View>

      <View style={styles.inputItem}>
        <MessageInput
          editable={true}
          keyName="message"
          placeholder="메세지를 입력해주세요"
          value={params.message}
          onTextChange={handleChangeText}
          onLeftPress={() => {}}
          onRightPress={() => {}}
        />
      </View>

      <View style={{ width: screenWidth }}>
        <SearchBar
          placeholder="회사명, 프로젝트명, 채팅내역을 검색하세요"
          keyName="search"
          value={params.search}
          editable
          onTextChange={handleChangeText}
          multiline
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: GrayColors.Gray300,
  },
  inputItem: { marginBottom: 10, minWidth: '80%' },
});
