export const CommonColors = {
  Back: '#000',
  White: '#fff',
};
export const TextColors = {
  Primary: '#343A40',
  Secondary: '#868E96',
  Disabled: '#CED4DA',
};
export const DividerColors = {
  Divider: '#F1F3F5',
};
export const GrayColors = {
  Gray100: '#F8F9FA',
  Gray200: '#F1F3F5',
  Gray300: '#E9ECEF',
  Gray400: '#E9ECEF', // light
  Gray500: '#CED4DA', // main
  Gray600: '#ADB5BD', // dark
  Gray700: '#868E96',
  Gray800: '#495057',
  Gray900: '#343A40',
  Gray1000: '#212529',
};
export const PrimaryColors = {
  Primary100: '#F4F8FF',
  Primary200: '#E9F1FF',
  Primary300: '#CADDFF',
  Primary400: '#8EB7FF',
  Primary500: '#3479FF', // disabled
  Primary600: '#125CED',
  Primary700: '#094BCA', // secondary
  Primary800: '#1042A3',
  Primary900: '#12367C', // primary
  Primary1000: '#1A2A4A',
};
export const ErrorColors = {
  Background: 'rgba(229, 55, 52, 0.04)', // #E53935, 4%
  Light: '#FCE7E7',
  Base: '#F88B88',
  Main: '#E53935',
  Dark: '#C62828',
};
export const WarningColors = {
  Background: 'rgba(255, 193, 5, 0.04)', // #FFC107,4%
  Light: '#FFF1C8',
  Base: '#FFDA6B',
  Main: '#FFC107',
  Dark: '#FFA000',
};
export const SuccessColors = {
  Background: 'rgba(34, 195, 69, 0.04)', // #22C245, 4%
  Light: '#E8F5E9',
  Base: '#8AD78D',
  Main: '#22C245',
  Dark: '#06A228',
};
