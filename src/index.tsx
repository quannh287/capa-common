import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import { SnackBar } from './Components/Common';
import { InputGroup } from './Screens/InputGroup';
import { CommonColors } from './Utils';

const App = () => {
  return (
    <SafeAreaView style={styles.container}>
      {/* Testing webview */}
      {/* <WebView
        source={{
          uri: 'https://hoops.capa.ai/viewer.html?modelId=https://capa-api.capa.ai/cloud/0c4c65a7-49a2-48ed-8∏9b6-524bf9758e75.scs',
        }}
        style={styles.webView}
      /> */}

      {/* <InputGroup /> */}
      <SnackBar
        text="회사명, 프로젝트명, 채팅내역을 검색하세요 회사명, 프로젝트명,
            채팅내역을 검색하세요"
      />
    </SafeAreaView>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  webView: {
    flex: 1,
    backgroundColor: CommonColors.White,
  },
});
