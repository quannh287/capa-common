export enum Status {
    inactived = 0,
    actived,
    focused,
    disabled,
    error,
}
