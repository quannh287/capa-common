import { EYE_CLOSE_ICON, EYE_OPEN_ICON } from '@/Assets';
import { Fonts } from '@/Assets/Fonts';
import { CommonColors, ErrorColors, GrayColors, TextColors } from '@/Utils';
import React, { useState } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { TextFieldProps } from './TextField';

export interface PasswordFieldProps extends TextFieldProps {}

export function PasswordField({
  keyName,
  value,
  placeholder,
  multiline,
  editable,
  iconRight,
  keyboardType,
  required,
  errorMsg,
  onTextChange,
  ...restProps
}: PasswordFieldProps) {
  const [secureText, setSecureText] = useState<boolean>(true);
  const [isActive, setIsActive] = useState<boolean>(false);
  const onFocus = () => {
    setIsActive(true);
  };
  const onBlur = () => {
    setIsActive(false);
  };
  const onChange = (_value: any) => {
    onTextChange(keyName, _value);
  };

  const onRightPress = () => {
    setSecureText(!secureText);
  };

  const textFieldStyle = {
    borderColor: isActive ? TextColors.Primary : GrayColors.Gray100,
    backgroundColor: isActive ? CommonColors.White : GrayColors.Gray100,
  };
  const textFieldErrorStyle = {
    borderColor: ErrorColors.Main,
    backgroundColor: ErrorColors.Background,
  };

  return (
    <View style={[styles.container, { opacity: editable ? 1 : 0.5 }]}>
      <View
        style={[
          styles.textField,
          errorMsg ? textFieldErrorStyle : textFieldStyle,
        ]}
      >
        <TextInput
          style={styles.textInput}
          placeholder={placeholder}
          placeholderTextColor={GrayColors.Gray600}
          multiline={multiline}
          value={value}
          onChangeText={onChange}
          keyboardType={keyboardType}
          secureTextEntry={secureText}
          onFocus={onFocus}
          onBlur={onBlur}
          editable={editable}
          underlineColorAndroid="transparent"
        />

        <TouchableOpacity style={styles.icon} onPress={onRightPress}>
          <Image
            source={secureText ? EYE_OPEN_ICON : EYE_CLOSE_ICON}
            style={{ tintColor: TextColors.Secondary }}
          />
        </TouchableOpacity>
      </View>

      {errorMsg && errorMsg?.length > 0 ? (
        <View style={styles.error}>
          <Text style={styles.errorText}>{errorMsg}</Text>
        </View>
      ) : null}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  textField: {
    borderWidth: 1,
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInput: {
    flex: 1,
    color: TextColors.Primary,
    minHeight: 45,
    paddingLeft: 16,
  },
  icon: {
    width: 45,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
  error: {
    marginTop: 3,
  },
  errorText: {
    color: ErrorColors.Main,
    fontFamily: Fonts.Regular,
    fontSize: 12,
    letterSpacing: -0.7,
  },
});
