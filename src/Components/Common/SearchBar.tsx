import { ARROW_LEFT_ICON, CLEAR_ICON, SEARCH_ICON } from '@/Assets';
import { CommonColors, GrayColors, TextColors } from '@/Utils';
import React, { useCallback, useRef, useState } from 'react';
import {
  Image,
  KeyboardType,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { IconButton } from './IconButton';

interface SearchBarProps {
  keyName: string;
  value: string;
  placeholder: string;
  multiline?: boolean;
  editable: boolean;
  keyboardType?: KeyboardType;
  onTextChange: (keyName: string, value: string) => void;
}

export const SearchBar = ({
  keyName,
  value,
  placeholder,
  multiline,
  keyboardType,
  onTextChange,
}: SearchBarProps) => {
  const inputRef = useRef<TextInput>(null);
  const [isActive, setIsActive] = useState<boolean>(false);

  const focus = useCallback(() => {
    inputRef.current && inputRef.current.focus();
  }, [inputRef]);

  const onChange = (_value: any) => {
    onTextChange(keyName, _value);
  };
  const onFocus = () => {
    setIsActive(true);
  };
  const onBlur = () => {
    setIsActive(false);
  };
  const clearText = () => {
    onChange('');
  };

  return (
    <View style={styles.container}>
      <View style={styles.backSection}>
        <IconButton icon={ARROW_LEFT_ICON} onPress={() => {}} />
      </View>

      <View style={styles.inputSection}>
        <View style={styles.row}>
          <TouchableOpacity style={styles.icon} onPress={focus}>
            <Image
              source={SEARCH_ICON}
              style={{ tintColor: TextColors.Secondary }}
            />
          </TouchableOpacity>

          <TextInput
            style={[styles.textInput]}
            ref={inputRef}
            placeholder={placeholder}
            placeholderTextColor={GrayColors.Gray600}
            multiline={multiline}
            value={value}
            onChangeText={onChange}
            keyboardType={keyboardType}
            onFocus={onFocus}
            onBlur={onBlur}
            secureTextEntry={false}
            underlineColorAndroid="transparent"
            editable={true}
          />

          {value.length > 0 && (
            <TouchableOpacity style={styles.clearIcon} onPress={clearText}>
              <Image
                source={CLEAR_ICON}
                style={{ tintColor: TextColors.Secondary }}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 4,
    paddingRight: 8,
  },
  backSection: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputSection: {
    flex: 1,
  },

  row: {
    backgroundColor: CommonColors.White,
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 8,
  },
  textInput: {
    flex: 1,
    color: TextColors.Primary,
    height: 40,
    letterSpacing: -0.7,
  },
  icon: {
    width: 36,
    height: 36,
    justifyContent: 'center',
    alignItems: 'center',
  },
  clearIcon: {
    marginRight: 8,
    width: 20,
    height: 20,
    borderRadius: 20,

    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: GrayColors.Gray300,
  },
});
