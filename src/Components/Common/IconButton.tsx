import { CommonColors, GrayColors, PrimaryColors } from '@/Utils';
import React from 'react';
import {
  Image,
  ImageSourcePropType,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';

export interface IconButtonProps {
  icon: ImageSourcePropType;
  color?: string;
  backgroundColor?: string;
  disabled?: boolean;
  onPress: () => void;
}

export function IconButton({
  icon,
  color = CommonColors.Back,
  backgroundColor,
  disabled = false,
  onPress,
}: IconButtonProps) {
  return (
    <TouchableHighlight
      underlayColor={GrayColors.Gray100}
      style={[styles.container, { backgroundColor }]}
      onPress={onPress}
      disabled={disabled}
    >
      <Image source={icon} style={[styles.icons, { tintColor: color }]} />
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',

    borderRadius: 32,
    width: 32,
    height: 32,
  },
  icons: {},
  text: {
    textAlign: 'center',
    letterSpacing: -0.3,
    paddingHorizontal: 50,
    paddingVertical: 5,
  },
});
