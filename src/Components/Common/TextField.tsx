import { Fonts } from '@/Assets/Fonts';
import { CommonColors, ErrorColors, GrayColors, TextColors } from '@/Utils';
import React, { useCallback, useRef, useState } from 'react';
import {
  Image,
  ImageSourcePropType,
  KeyboardType,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

export interface TextFieldProps {
  keyName: string;
  value: string;
  placeholder: string;
  multiline?: boolean;
  editable: boolean;
  iconLeft?: ImageSourcePropType;
  iconRight?: ImageSourcePropType;
  keyboardType?: KeyboardType;
  required?: boolean;
  errorMsg?: string;
  backgroundColor?: string;
  onTextChange: (keyName: string, value: string) => void;
  onRightPress?: () => void;
  onLeftPress?: () => void;
}

export function TextField({
  keyName,
  value,
  placeholder,
  multiline,
  editable,
  iconLeft,
  iconRight,
  keyboardType,
  required,
  errorMsg,
  backgroundColor = GrayColors.Gray100,
  onTextChange,
  onLeftPress,
  onRightPress,
  ...restProps
}: TextFieldProps) {
  const inputRef = useRef<TextInput>(null);
  const [isActive, setIsActive] = useState<boolean>(false);

  const focus = useCallback(() => {
    inputRef.current && inputRef.current.focus();
  }, [inputRef]);
  const blur = useCallback(() => {
    inputRef.current && inputRef.current.blur();
  }, [inputRef]);

  const onFocus = () => {
    setIsActive(true);
  };
  const onBlur = () => {
    setIsActive(false);
  };
  const onChange = (_value: any) => {
    onTextChange(keyName, _value);
  };

  const textFieldStyle = {
    borderColor: isActive ? TextColors.Primary : GrayColors.Gray100,
    backgroundColor: isActive ? CommonColors.White : GrayColors.Gray100,
  };
  const textFieldErrorStyle = {
    borderColor: ErrorColors.Main,
    backgroundColor: ErrorColors.Background,
  };

  return (
    <View style={[styles.container, { opacity: editable ? 1 : 0.5 }]}>
      <View
        style={[
          styles.textField,
          { backgroundColor },
          errorMsg ? textFieldErrorStyle : textFieldStyle,
        ]}
      >
        {iconLeft && (
          <TouchableOpacity style={styles.icon} onPress={onLeftPress}>
            <Image
              source={iconLeft}
              style={{ tintColor: TextColors.Secondary }}
            />
          </TouchableOpacity>
        )}

        <TextInput
          style={[
            styles.textInput,
            iconLeft === undefined ? { paddingLeft: 16 } : null,
          ]}
          ref={inputRef}
          placeholder={placeholder}
          placeholderTextColor={GrayColors.Gray600}
          multiline={multiline}
          value={value}
          onChangeText={onChange}
          keyboardType={keyboardType}
          secureTextEntry={false}
          onFocus={onFocus}
          onBlur={onBlur}
          underlineColorAndroid="transparent"
          editable={editable}
        />

        {iconRight && (
          <TouchableOpacity style={styles.icon} onPress={onRightPress}>
            <Image
              source={iconRight}
              style={{ tintColor: TextColors.Secondary }}
            />
          </TouchableOpacity>
        )}
      </View>

      {/* ERROR MSG */}
      {errorMsg && errorMsg?.length > 0 ? (
        <View style={styles.error}>
          <Text style={styles.errorText}>{errorMsg}</Text>
        </View>
      ) : null}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  textField: {
    borderWidth: 1,
    borderColor: '#343A40',
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInput: {
    flex: 1,
    color: TextColors.Primary,
    minHeight: 45,
  },
  icon: {
    padding: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  error: {
    marginTop: 3,
  },
  errorText: {
    color: ErrorColors.Main,
    fontFamily: Fonts.Regular,
    fontSize: 12,
    letterSpacing: -0.7,
  },
});
