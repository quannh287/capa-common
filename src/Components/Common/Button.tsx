import { Fonts } from '@/Assets/Fonts';
import { CommonColors, GrayColors, PrimaryColors } from '@/Utils';
import React from 'react';
import {
  Image,
  ImageSourcePropType,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';

export interface ButtonProps {
  text: string;
  color?: string;
  disabled?: boolean;
  fontSize?: number;
  fontWeight?: 'thin' | 'light' | 'regular' | 'medium' | 'bold';
  onPress?: () => void;
  iconLeft?: ImageSourcePropType;
  iconRight?: ImageSourcePropType;
}

export function Button({
  text,
  color = CommonColors.White,
  fontSize = 16,
  disabled = false,
  onPress,
  iconLeft,
  iconRight,
}: ButtonProps) {
  let backgroundColor = disabled
    ? GrayColors.Gray500
    : PrimaryColors.Primary500;

  return (
    <TouchableHighlight
      underlayColor={PrimaryColors.Primary600}
      style={[styles.container, { backgroundColor }]}
      onPress={onPress}
      disabled={disabled}
    >
      <View style={styles.row}>
        {iconLeft && (
          <Image
            source={iconLeft}
            style={[styles.icons, { tintColor: color }]}
          />
        )}

        <Text
          style={[styles.text, { fontSize, fontFamily: Fonts.Regular, color }]}
        >
          {text}
        </Text>

        {iconRight && (
          <Image
            source={iconRight}
            style={[styles.icons, { tintColor: color }]}
          />
        )}
      </View>
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',

    borderRadius: 4,
    paddingVertical: 6,
    paddingHorizontal: 16,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  icons: {
    minWidth: 5,
    minHeight: 5,
  },
  text: {
    textAlign: 'center',
    letterSpacing: -0.3,
    paddingHorizontal: 10, // xem lai
    paddingVertical: 5, // xem lai
  },
});
