import { CHECK_ICON, USER_CIRCLE_OUTLINE_ICON } from '@/Assets';
import { Fonts } from '@/Assets/Fonts';
import { CommonColors, TextColors } from '@/Utils';
import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

interface SnackBarProps {
  text: string;
}

export const SnackBar: React.FunctionComponent<SnackBarProps> = ({ text }) => {
  return (
    <View style={styles.container}>
      <View style={styles.contentRow}>
        <View style={styles.contentIcon}>
          <Image source={CHECK_ICON} style={styles.contentIconImg} />
        </View>

        <View style={styles.contentRight}>
          <Text style={styles.contentText}>{text}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: TextColors.Primary,
    borderRadius: 8,
    padding: 16,
  },
  contentRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
  },
  contentIcon: {},
  contentIconImg: {
    tintColor: CommonColors.White,
    width: 24,
    height: 24,
  },
  contentRight: {
    flex: 1,
  },
  contentText: {
    marginLeft: 16,
    fontFamily: Fonts.Regular,
    fontSize: 16,
    lineHeight: 24,
    color: CommonColors.White,
    letterSpacing: -0.3,
  },
});
