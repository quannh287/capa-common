import { Platform } from 'react-native';

export const Fonts = {
  Thin: Platform.OS === 'ios' ? 'SpoqaHanSansNeo-Thin' : 'SpoqaHanSansNeo-Thin',
  Light: Platform.OS === 'ios' ? 'SpoqaHanSansNeo-Light' : 'SpoqaHanSansNeo-Light',
  Regular: Platform.OS === 'ios' ? 'SpoqaHanSansNeo-Regular' : 'SpoqaHanSansNeo-Regular',
  Medium: Platform.OS === 'ios' ? 'SpoqaHanSansNeo-Medium' : 'SpoqaHanSansNeo-Medium',
  Bold: Platform.OS === 'ios' ? 'SpoqaHanSansNeo-Bold' : 'SpoqaHanSansNeo-Bold',
};
